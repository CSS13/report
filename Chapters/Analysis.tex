% Audio Tag capabilities
\section{HTML5 Audio Tag}
\label{sec:HTML5_audio_tag}

Adding support for music file types not already supported by the browser 
would be an major undertaking, 
and would not contribute much towards the goals of this project, 
so we have decided to make use of existing browser capabilities.
\newline\newline
Most modern browsers support the \acs{HTML}5 standard Audio tag,
also called \acs{HTML}5 Audio and WebAudio,
which presents a simple \acs{GUI} and audio playback to the user.
Browsers also support a similar Video tag, 
which could also be used for audio playback purposes.
The audio and video tags support both files and streams, 
allowing playback while a file is still being downloaded.
\newline
Supported audio codings differ greatly between browsers:
Firefox and Opera do not support the proprietary formats Mp3, Mp4 or ADTS
without additional software on the machine.
Safari does not support the free open source formats WebM or Ogg,
and Internet Explorer and Microsoft Edge does not support anything other than Mp3 and Mp4.
Google chrome notably supports all the above formats.
\newline

%\includegraphics{gfx/audioTag}
\begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{gfx/audioTag.jpg}
    \caption{A picture of Audio Tag \acs{GUI} in Firefox}
    \label{fig:audiotag}
\end{figure}

The audio tag itself offers a very simplistic yet very inflexible \acs{GUI} to the user, 
it includes the usual progress bar, volume, play+pause buttons in its interface, 
these elements cannot be easily changed, but can be disabled and hidden entirely, 
which lets the audio playback be controlled by JavaScript code instead.

% Song Information
\section{Music File-types}
The supported music file types differ greatly from each other internally; 
their audio codings can be different, their meta-data can be different,
and even the way they are rendered and treated by the browsers can differ.
To make matters even worse creators of music files are free to exclude or falsify meta-data, 
and can even leave it out entirely.
Clearly, some kind of common abstraction is needed to encapsulate the uncertain terrain of music files.

\section{WebRTC limitations}
% WebRTC limitations
WebTorrent relies heavily on WebRTC and particularly on WebRTCs dataChannels.
DataChannels are not supported in the Internet Explorer and Safari browsers, 
so these cannot support WebTorrent either.

Creating a data channel is a performance intensive task:
Peers have to contact a signaling service to inform a remote peer of its intention
to make a peer-to-peer connection, both peers have to perform NAT traversal to establish routing information 
and create necessary firewall arrangements, share their routing information,
and finally agree on protocols, encodings and so on.
RTC datachannels have a large overhead when being established,
but can perform low latency communications once established, 
thus we would like to retain datachannels for as long as possible to avoid this overhead.
Many \acs{DHT}, like Kademlia, will establish and discard connections frequently,
and might suffer major overhead when implemented in WebRTC over the datachannel.`

% LocalStorage
\section{Storage}
Ensuring availability of content and robustness in a BitTorrent network requires persistent offline data storage:
without persistent data storage, if all users were to leave the network and return later, all content on the 
network would be lost.
\newline\newline
Until recently the only way to have data persistency through browsers has been cookies, 
which allows saving up to 4KB in each cookie, and can hold at least 50 cookies per domain, 
which allows for a total of 200KB of guaranteed available storage, possibly more depending on the browser.
\newline\newline
Seeing the need for larger amounts of persistent data, browsers now implement Web Storage 
(also known as DOM storage), a new standard by W3C (see \citep{WebStorage})
which consists of sessionStorage and localStorage.

Mozilla also defines a globalStorage for their Firefox browser, 
but this is not supported by any other major browsers, so we will disregard it for this project.

SessionStorage is intended to be wiped whenever the current session ends, 
even if the user agent has not requested it, making it also unsuited for this project.

Data stored using LocalStorage is only wiped when explicitly requested by the user agent or 
by some security policy specified by the user agent beforehand.
\newline\newline
The current Web Storage specification draft is somewhat ambiguous, 
it suggests that browsers implement support for at least 5MB of storage of each kind per domain,
but the actual support varies wildly even between versions of the same browser:

\begin{table}[h]
	\centering
	\begin{tabular}{l | r}
        Browser   & Storage limit \\ \hline
		Chrome 18 & No limit  \\
		Chrome 19 & 1    MB   \\
		Chrome 36 & 5    MB   \\
		Chrome 46 & 4.75 MB   \\
		Firefox   & 5    MB   \\
		IE        & 10   MB   \\
		Safari    & 5    MB   \\
	\end{tabular}
	\caption{Table of size limits of localstorage}
	\label{table:browserls}
\end{table}

Additionally, Firefox allows the user to specify any LocalStorage maximum size, even zero. 
Google have attempted to implement a quota \acs{API}, 
which allows websites to ask user agents to increase localstorage limitations,
this has not been adopted by the other browsers, so we had to look for something else.
\newline\newline
In addition to the Web Storage standard, W3C is also working on a standard for IndexedDB in browsers, 
which allows browser-side JavaScript to access a local database on the host machine.

It should be noted that WebTorrent, 
an essential library in our project, 
only officially supports Firefox and Chrome 
both of which do support IndexedDB.

\section{BEP - BitTorrent Enhancement Proposals}
%bep
BitTorrent enhancement proposals is a place where users of BitTorrent can come with proposals to improve the protocol.
One of these proposals focuses on \acs{HTTP} seeding \citep{bittorrent:bep17}.
This is relevant in our work, because we focus on how we can make streaming work in the browser. The proposal is about changing the meta-data file to include a \acs{HTTP} seeds key. This key would refer to a list of web addresses where the torrent data can be downloaded from.
If support for \acs{HTTP} seeds would be added to the BitTorrent protocol it would mean that there is more uptime of seeds because the ftp and \acs{HTTP} server would always be up.

In another proposal \citep{bittorrent:bep09} it is proposed to make it possible for clients to join a swarm and complete a download without having to download a .torrent file first, from an info hash contained in magnet links. The gains from using magnet links over torrents are that information about the torrent can be downloaded directly from other peers. Regular torrent files needs to be placed upon a web-server where it takes up space. The server must be up and running for the users to get information about the torrent, and that is not needed with using magnet links.
Now users can just share a link instead of sharing a file on a server.

Private torrents is another feature in BitTorrent \citep{bittorrent:bep27}. They idea is that users can define a torrent to be private by setting 'private=1' in the meta-info (.torrent) file. Private trackers can be used to control what users are allowed to download a torrent. The tracker is a server with a list of the peers that contain pieces of the torrent and a list of users allowed to get the specific torrent.
The tracker will refuse to provide a seed-list to those not allowed to get that torrent.

%Storing arbitrary data in the \acs{DHT}
In a proposal \citep{bittorrent:bep44} it is suggested that the \acs{DHT} of BitTorrent is extended with the possibility of storing arbitrary data instead of just storing key-value pairs with hashes and associated \acs{IP}-addresses.
With this change the \acs{DHT} could be used to store information about the torrent e.g. song length, author, file size, etc.

%Why browser torrents
\section{Browser torrents}
The smart thing about torrents in the browser is that we are not dependent of a server to serve the files, and users does not have to download and install a client to use our solution.

%Scraping
\section{Scraping}
Scraping is a request to the tracker to get information about a torrent, such as how many seeders and leachers it has, how many times it has been downloaded, the status of the tracker (OK or offline), the reason it is offline, etc.
Scraping can be used to find the torrents with bad health and add them to a list. Each peer can then get the list from the \acs{DHT} and seed the torrents on the list.
This can be used to get the health of a torrent i.e. how many seeds it has, and if it at bad health we can add it to a list, and use that list so nodes in the network can download and seed those torrents in the list and help ensure availability of the less popular content.
